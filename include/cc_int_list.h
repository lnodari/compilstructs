// Copyright (c) 2016 Lucas Nodari 
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CC_INT_LIST_H_
#define CC_INT_LIST_H_

typedef struct comp_int_list_item {
	int value;
	struct comp_int_list_item *next,*prev;
} comp_int_list_item_t;

typedef struct comp_int_list {
	int size;
	comp_int_list_item_t *first,*last;
} comp_int_list_t;

comp_int_list_t* int_list_new();
void int_list_free();

void int_list_push(comp_int_list_t *list, int value);
int int_list_remove(comp_int_list_t *list, int value);
int int_list_is_empty(comp_int_list_t *list);

int int_list_compare(comp_int_list_t *first, comp_int_list_t *second);
int int_list_product(comp_int_list_t *list);
int int_list_sum(comp_int_list_t *list);
int int_list_gt_zero(comp_int_list_t *list);

void int_list_print(comp_int_list_t *list);
void int_list_print_hex(comp_int_list_t *list);
void int_list_print_types(comp_int_list_t *list);

#endif // CC_INT_LIST_H_
