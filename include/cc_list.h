// Copyright (c) 2016 Lucas Nodari 
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CC_LIST_H_
#define CC_LIST_H_

typedef struct comp_list_item {
	void *value;
	struct comp_list_item *next,*prev;
} comp_list_item_t;

typedef struct comp_list {
	int size;
	comp_list_item_t *first,*last;
} comp_list_t;

/*
 * Função: list_new, Cria uma lista vazia, aborta a execução
 * do programa caso algum erro de alocação de memória tenha ocorrido.
 */
comp_list_t* list_new();

/*
 * Função: list_free, Libera toda memória alocada para a lista,
 * estruturas armazenadas devem ser liberadas externamente, previamente
 * ao uso desta função.
 */
void list_free();

/*
 * Função: list_push_back, Insere value no final da lista, aborta a
 * execução caso a lista não tenha sido inicializada.
 */
void list_push_back(comp_list_t *list, void *value);

/*
 * Função: list_push_back, Insere value no inicio da lista, aborta a
 * execução caso a lista não tenha sido inicializada.
 */
void list_push_front(comp_list_t *list, void *value);

/*
 * Função: list_pop_front, Remove e retorna o primeiro elemento
 * da lista, aborta a execução do programa caso a lista não tenha
 * sido inicializada ou esteja vazia.
 */
void* list_pop_front(comp_list_t *list);

/*
 * Função: list_remove, Remove da lista o elemento que contém
 * value, retorna 1 caso elemento tenha sido removido, 0 caso
 * contrário, aborta a execução do programa caso a lista não tenha
 * sido inicializada ou esteja vazia.
 */
int list_remove(comp_list_t *list, void *value);

/*
 * Função: list_is_empty, Retorna 1 se lista estiver vazia, 0 caso
 * contrário.
 */
int list_is_empty(comp_list_t *list);

/*
 * Função: list_debug_print, Percorre a lista e exibe os ponteiros
 * dos atributos value.
 */
void list_debug_print(comp_list_t *list);

#endif // CC_INT_LIST_H_
