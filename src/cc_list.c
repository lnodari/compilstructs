// Copyright (c) 2016 Lucas Nodari 
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdio.h>

#include "cc_list.h"

#define ERRO(MENSAGEM) { fprintf (stderr, "[cc_list, %s] %s.\n", __FUNCTION__, MENSAGEM); abort(); }

comp_list_t* list_new(){
	comp_list_t *list = malloc(sizeof(comp_list_t));
	if (!list)
		ERRO("Failed to allocate memory for list");

	list->size = 0;
	list->first = NULL;
	list->last = NULL;
	return list;
}

void list_free(comp_list_t *list){
	comp_list_item_t *current = list->first;
	comp_list_item_t *next;
	do {
		next = current->next;
		free(current);
		current = next;
	} while(current != NULL);
	free(list);
}

void list_push_back(comp_list_t *list, void *value){
	if (list == NULL)
		ERRO("Cannot insert element on an uninitialized list");

	comp_list_item_t *item = malloc(sizeof(comp_list_item_t));
	if (!item)
		ERRO("Failed to allocate memory for list item");

	item->value = value;
	item->next = NULL;
	item->prev = NULL;

	if (list_is_empty(list)){
		list->first = item;
		list->last = item;
	} else {
		item->prev = list->last;
		list->last->next = item;
		list->last = item;
	}
	++list->size;
}

void list_push_front(comp_list_t *list, void *value){
	if (list == NULL)
		ERRO("Cannot insert element on an uninitialized list");

	comp_list_item_t *item = malloc(sizeof(comp_list_item_t));
	if (!item)
		ERRO("Failed to allocate memory for list item");

	item->value = value;
	item->next = NULL;
	item->prev = NULL;

	if (list_is_empty(list)){
		list->first = item;
		list->last = item;
	} else {
		item->next = list->first;
		list->first->prev = item;
		list->first = item;
	}
	++list->size;
}

void* list_pop_front(comp_list_t *list){
	if (list == NULL)
		ERRO("Cannot remove element from an uninitialized list");
	if (list_is_empty(list))
		ERRO("Cannot remove element from an empty list");

	comp_list_item_t *item = list->first;

	if (list->size == 1){
		list->first = NULL;
		list->last = NULL;
	} else {
		list->first->next->prev = NULL;
		list->first = list->first->next;
	}

	if (item){
		item->next = NULL;
		item->prev = NULL;
	}

	--list->size;
	return item->value;
}

int list_remove(comp_list_t *list, void *value){
	if (list == NULL)
		ERRO("Cannot remove element from an uninitialized list");
	if (list_is_empty(list))
		ERRO("Cannot remove element from an empty list");

	comp_list_item_t *item = NULL;

	if (list->size == 1){
		if (list->first->value == value){
			item = list->last;
			list->first = NULL;
			list->last = NULL;
		}
	} else if (list->first->value == value){
		item = list->first;
		list->first = item->next;
		list->first->prev = NULL;
	} else if (list->last->value == value){
		item = list->last;
		list->last = item->prev;
		list->last->next = NULL;
	} else {
		item = list->first;
		do {
			item = item->next;
		} while(!(item == NULL || item->value == value));

		if (item == NULL)
			return 0;
		item->prev->next = item->next;
		item->next->prev = item->prev;
	}

	--list->size;
	item->next = NULL;
	item->prev = NULL;
	free(item);
	return 1;
}

int list_is_empty(comp_list_t *list){
	return (list->size == 0);
}

void list_debug_print(comp_list_t *list){
	if (list == NULL || list_is_empty(list))
		return;

	comp_list_item_t *current = list->first;
	int index = 0;
	do {
		printf("%d: %p\n",index++,current->value);
		current = current->next;
	} while(current != NULL);
}

