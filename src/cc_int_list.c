// Copyright (c) 2016 Lucas Nodari 
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdio.h>

#include "cc_int_list.h"

comp_int_list_t* int_list_new(){
	comp_int_list_t *list = malloc(sizeof(comp_int_list_t));
	list->size = 0;
	list->first = NULL;
	list->last = NULL;
	return list;
}

void int_list_free(comp_int_list_t *list){
	comp_int_list_item_t *current = list->first;
	comp_int_list_item_t *next;
	do {
		next = current->next;
		free(current);
		current = next;
	} while(current != NULL);
	free(list);
}

void int_list_push(comp_int_list_t *list, int value){
	if (list == NULL)
		return;

	comp_int_list_item_t *item = malloc(sizeof(comp_int_list_item_t));
	item->value = value;
	item->next = NULL;
	item->prev = NULL;

	if (int_list_is_empty(list)){
		list->first = item;
		list->last = item;
	} else {
		item->prev = list->last;
		list->last->next = item;
		list->last = item;
	}
	++list->size;
}

int int_list_remove(comp_int_list_t *list, int value){
	if (list == NULL || int_list_is_empty(list))
		return 0;

	comp_int_list_item_t *item = NULL;

	if (list->size == 1){
		if (list->first->value == value){
			item = list->last;
			list->first = NULL;
			list->last = NULL;
		}
	} else if (list->first->value == value){
		item = list->first;
		list->first = item->next;
		list->first->prev = NULL;
	} else if (list->last->value == value){
		item = list->last;
		list->last = item->prev;
		list->last->next = NULL;
	} else {
		item = list->first;
		do {
			item = item->next;
		} while(!(item == NULL || item->value == value));

		if (item == NULL)
			return 0;
		item->prev->next = item->next;
		item->next->prev = item->prev;
	}

	--list->size;
	item->next = NULL;
	item->prev = NULL;
	free(item);
	return 1;
}

int int_list_is_empty(comp_int_list_t *list){
	return (list->size == 0);
}

// retorna o numero do primeiro elemento diferente
int int_list_compare(comp_int_list_t *first, comp_int_list_t *second){
	if (first->size != second->size) // condição deve ser verificada e tratada externamente
		return -1;

	comp_int_list_item_t *a = first->first;
	comp_int_list_item_t *b = second->first;

	int i = 1;
	while (a->value == b->value && a != NULL && b != NULL){
		a = a->next;
		b = b->next;
		++i;
	}

	return i;
}

int int_list_product(comp_int_list_t *list){
	comp_int_list_item_t *current = list->first;
	int result = 1;
	do {
		result *= current->value;
		current = current->next;
	} while(current != NULL);
	return result;
}

int int_list_sum(comp_int_list_t *list){
	if (list == NULL) return 0;

	comp_int_list_item_t *current = list->first;
	int result = 0;
	do {
		result += current->value;
		current = current->next;
	} while(current != NULL);
	return result;
}

// verifica se todos elementos são maior que zero
int int_list_gt_zero(comp_int_list_t *list){
	comp_int_list_item_t *current = list->first;
	int result = 1;
	do {
		result &= (current->value > 0);
		current = current->next;
	} while(current != NULL);
	return result;
}

void int_list_print(comp_int_list_t *list){
	if (list == NULL || int_list_is_empty(list))
		return;

	comp_int_list_item_t *current = list->first;
	do {
		printf(" %d",current->value);
		current = current->next;
	} while(current != NULL);
}

void int_list_print_hex(comp_int_list_t *list){
	if (list == NULL || int_list_is_empty(list))
		return;

	comp_int_list_item_t *current = list->first;
	int i = 0;
	do {
		printf("%d: %#x\n",i++,current->value);
		current = current->next;
	} while(current != NULL);
}

